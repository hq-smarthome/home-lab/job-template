# Job Template

[[_TOC_]]

## Description

A generic gitlab ci & nomad job templates for hQ which was heavily inspired by the work done at the
[Internet Archive - Nomad](https://gitlab.com/internetarchive/nomad).

## Basic Usage

The absolute basic usage involves adding two lines to a `.gitlab-ci.yml` file in a repo you want to
deploy:

```yaml
include:
- remote: 'https://gitlab.com/hq-smarthome/home-lab/job-template/-/raw/main/.gitlab-ci.yml'
```

This will run a 4 stage deployment where the following will occur:

1. `render` - A Levant variable file will be populated with some defaults and basic deployment info. These will rendered into your custom `job.template.nomad` file which should be placed in the root of your repo. 
2. `validate` - Will trigger a static validation of the job configurations.
3. `plan` - Will trigger a plan within the nomad cluster to ensure it can be deployed. If it passes a check index will be saved (should help to prevent colliding parallel deployments)
4. `deploy` - Deploys the rendered job file to the nomad cluster using the check index. 

## Customised Jobs

The templates can be customised by adding additional info to the `.gitlab-ci.yml` file.

### Dynamic variables

You can provide the render stage extra variables to be used by Levant when rending the job file.
This can be done by adding a `before_script` in the render stage like so:

```yaml
include:
- remote: 'https://gitlab.com/hq-smarthome/home-lab/job-template/-/raw/main/.gitlab-ci.yml'

render:
  before_script:
  - 'echo "someVariable: theValueHere" >> "$LEVANT_VAR_FILE"'

```

This variable can then be used within the job file like:

```hcl
job "blah" {
  meta {
    someValue = "[[ .someVariable ]]"
  }
}
```

### Test / Build stages

There are two predefined stages are present within the template (not used by default) which allow for testing and building of applications before deployment (for instance building a docker image).

```yaml
include:
- remote: 'https://gitlab.com/hq-smarthome/home-lab/job-template/-/raw/main/.gitlab-ci.yml'

my-test-stage:
  stage: test
  ...other gitlab-ci options here

my-build-stage:
  stage: build
  ...other gitlab-ci options here
```

The full stage order is:

1. `test`
2. `build`
3. `publish`
4. `validate`
5. `plan`
6. `deploy`
